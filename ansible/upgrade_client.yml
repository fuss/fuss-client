#! /usr/bin/env ansible-playbook

# Copyright (C) 2016-2017 Progetto Fuss <info@fuss.bz.it>
#                         Elena Grandi <elena@truelite.it>,
#                         Christopher R. Gabriel <cgabriel@truelite.it>
# Copyright (C) 2017-2021 The FUSS Project <info@fuss.bz.it>
# Copyright (C) 2021-2022 The FUSS Project <info@fuss.bz.it>
# Author:  Elena Grandi <elena@truelite.it>,
#          Christopher R. Gabriel <cgabriel@truelite.it>,
#          Simone Piccardi <piccardi@truelite.it>
#          Marco Marinello <contact-nohuman@marinello.bz.it>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA

# This playbook is designed to be run from fuss-manager rather than locally,
# and is installed by the package fuss-manager-fuss-client

---
- name: Upgrade fuss-client on a client
  hosts: all
  vars_files:
      - fuss_zones.yaml
  roles:
      - wait_disable_upgrades
      - common
      - network
      - fuss_packages
      - homes
      - desktop
      - users
      - cluster
      - ldap
      - misc
      - ocsinventory
      - veyon
      - role: glpi
        when: fuss_zones[fuss_zone["fuss_zone"]]["glpi_server"] is defined
      # this role should be at the end because it re-enables the unattended
      # upgrades.
      - unattended-apt
  tasks:
      - name: Check that the facts directory exists
        file:
            path: '/etc/ansible/facts.d'
            state: directory
      - name: Get the current fuss-client version
        package_facts:
          manager: auto
      - name: get the fuss-client version
        set_fact:
          fuss_client_version: "{{ ansible_facts.packages['fuss-manager-fuss-client'][0]['version'] }}"
      - name: Save a timestamp of the current run in an ansible fact
        copy:
            dest: '/etc/ansible/facts.d/fuss_client.fact'
            content: |
                {
                    "timestamp": "{{ ansible_date_time.epoch }}",
                    "version": "{{ fuss_client_version }}"
                }
      - name: Remove the update trigger
        file:
            path: /var/lib/fuss-client/upgrade-trigger
            state: absent
