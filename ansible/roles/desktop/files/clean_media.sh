#!/bin/bash

umount /media/*/*

rmdir --ignore-fail-on-non-empty /media/*/*

for D in $(ls /media) ; do
    BASE=$(basename $D)
    if [[ $BASE != cdrom* ]] ; then
        rmdir --ignore-fail-on-non-empty /media/$BASE
    fi
done
