// Custom Firefox-esr configuration for fuss-client

// Use proxy autodetection
pref("network.proxy.type", 4);

// Autologin proxy if password is saved
pref("signon.autologin.proxy", true);

