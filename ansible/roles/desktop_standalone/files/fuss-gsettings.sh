#!/bin/bash

# ibus: set Italian and German keyboard layouts
gsettings set org.freedesktop.ibus.general preload-engines "['xkb:it::ita', 'xkb:de::deu']"
# ibus: set tray icon color to white
gsettings set org.freedesktop.ibus.panel xkb-icon-rgba "#FFFFFF"
