#!/bin/bash

for i in $(awk -F : -s '/^guest/{print $1}' /etc/passwd)
do
    echo "Cleaning guest user $i"
    userdel -r $i
done
