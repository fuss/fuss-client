��          D      l       �   �   �   C   q     �  L   �      ,  2  P   _     �  Z   �                          All data created during this guest session will be deleted
when you log out, and settings will be reset to defaults.
Please save files on some external device, for instance a
USB stick, if you would like to access them again later. Another alternative is to save files in the
/var/guest-data folder. Temporary Guest Session Warning: do not lock the session otherwise you won't be
able to get back in. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 Alle Daten, die während dieser Gastsitzung erstellt wurden, werden gelöscht,
wenn Sie sich abmelden, und die Einstellungen werden auf die Standardwerte zurückgesetzt.
Bitte speichern Sie Dateien auf einem externen Gerät, 
z. B. einem USB-Stick, wenn Sie später wieder auf sie zugreifen möchten. Eine andere Alternative ist, die Dateien im Ordner /var/guest-data zu speichern. Temporäre Gastsitzung Achtung: Sperren Sie die Sitzung nicht, sonst können Sie nicht wieder einsteigen können. 