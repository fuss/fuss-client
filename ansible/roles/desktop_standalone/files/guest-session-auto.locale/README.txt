Files in this directory will be installed by ansible to
/usr/local/share/locale/$LANG/LC_MESSAGES; to update translations change the
.po files, run make and please commit the generated files to the repository.
