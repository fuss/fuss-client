��          D      l       �   �   �   C   q     �  L   �      @  2  X   s     �  Q   �                          All data created during this guest session will be deleted
when you log out, and settings will be reset to defaults.
Please save files on some external device, for instance a
USB stick, if you would like to access them again later. Another alternative is to save files in the
/var/guest-data folder. Temporary Guest Session Warning: do not lock the session otherwise you won't be
able to get back in. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 Toutes les données créées pendant cette session invitée seront supprimées
lorsque vous vous déconnecterez, et les paramètres seront réinitialisés aux valeurs par défaut.
Veuillez enregistrer les fichiers sur un périphérique externe,
par exemple une clé USB,si vous souhaitez y accéder à nouveau plus tard. Une autre solution consiste à enregistrer les fichiers dans le dossier /var/guest-data. Session temporaire d'invité Attention : ne verrouillez pas la session, sinon vous ne pourrez plus y accéder. 