��          D      l       �   �   �   C   q     �  L   �      1  2  G   d     �  N   �                          All data created during this guest session will be deleted
when you log out, and settings will be reset to defaults.
Please save files on some external device, for instance a
USB stick, if you would like to access them again later. Another alternative is to save files in the
/var/guest-data folder. Temporary Guest Session Warning: do not lock the session otherwise you won't be
able to get back in. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 Todos los datos creados durante esta sesión de invitado se eliminarán
cuando cierre la sesión, y la configuración se restablecerá a los valores predeterminados.
Por favor, guarde los archivos en algún dispositivo externo, por ejemplo
una memoria USB, si desea volver a acceder a ellos más adelante. Otra alternativa es guardar los archivos en la carpeta /var/guest-data. Sesión de Invitados temporal Advertencia: no bloquee la sesión, de lo contrario no podrá volver a entrar. 