��          D      l       �   �   �   C   q     �  L   �      �   2  @   #     d  B   ~                          All data created during this guest session will be deleted
when you log out, and settings will be reset to defaults.
Please save files on some external device, for instance a
USB stick, if you would like to access them again later. Another alternative is to save files in the
/var/guest-data folder. Temporary Guest Session Warning: do not lock the session otherwise you won't be
able to get back in. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 Tutti i dati creati durante questa sessione guest saranno cancellati al logout, e le impostazioni verranno riportate al default.
Se si vuole conservare qualche file, è necessario salvarlo su un device esterno, ad esempio una chiavetta USB. Un'alternativa è salvare i file nella directory /var/guest-data Sessione guest temporanea Attenzione: bloccando la sessione sarà impossibile ripristinarla. 