#!/bin/bash
# This file is part of the FUSS Client Veyon integration.
# Copyright (C) 2019-2021 The FUSS Project <info@fuss.bz.it>
# Author: Claudio Cavalli <ccavalli-nohuman@fuss.bz.it>
# Copyright (C) 2022 The FUSS Project <info@fuss.bz.it>
# Author: Marco Marinello <contact-nohuman@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if [ -e /var/lib/fuss-client/veyon-excluded-groups ]; then
  EXCLUDED_GROUPS="$(xargs < /var/lib/fuss-client/veyon-excluded-groups)"
fi
if [ -z "$EXCLUDED_GROUPS" ]; then
  EXCLUDED_GROUPS="root docenti insegnanti veyon-master tecnici"
fi

# We don't want teachers to be monitored.
# Gather the groups the users just logged in belongs to.
# NB: The spaces are used for a more reliable detection
BELONGING_GROUPS=" $(id -nG $USER) "

for group in $EXCLUDED_GROUPS; do
  if echo "$BELONGING_GROUPS" | grep " $group " > /dev/null; then
    exit 0
  fi
done

/usr/bin/veyon-service &
