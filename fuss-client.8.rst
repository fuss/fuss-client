===========
fuss-client
===========

----------------------------------------
configure a workstation as a Fuss Client
----------------------------------------

SYNOPSIS
========
fuss-client [-h] [-a | -U | -S | -r | -l] [-t TIMEOUT]
            [-g CONFSERVERGROUP] [-s SERVERHOST] [-d DOMAIN]
            [-H HOSTNAME] [-u USER] [-k SSH_KEY] [-p] [--unofficial]
            [--iso] [--light] [--[no-]slick-greeter] [--locale LOCALE]
            [-v] [--debug] [--syslog] [--dryrun]

DESCRIPTION
===========

*fuss-client* is used to configure a workstation as a Fuss Client and
connect it to a Fuss Network

OPTIONS
=======

Main actions
-----------------

-a, --add
   Add the machine to the network

-U, --upgrade
   Update configuration for a machine already connected to a network

-S, --standalone
   Configure a standalone machine

-l, --listgroups
   List available clusters/groups on server

Configuration
------------------

-g GROUP, --groupjoin GROUP
   join the named cluster/group on server

-s SERVERHOST, --useserver SERVERHOST
   Provide the server hostname (if not supplied, automatic discovery
   will be tried)

-t TIMEOUT, --timeout TIMEOUT secs
   Timeout in server discovery (default to 5 seconds) - useful on slow
   networks

-d DOMAIN, --domain DOMAIN
   Local domain. Only specify if autodetection fails

-H HOSTNAME, --hostname HOSTNAME
   Set the local hostname before configuring the client
   (Incorporates the -a option)

--locale LOCALE
   Set locale for the machine; default is it_IT.UTF-8.

--keyboard KEYBOARD
   Set keyboard layout for the machine; default is it.

--unofficial
   Also include contrib and non-free.

--iso
   Ignore configurations that don't work in a chroot

--light
   Skip installing heavyweight dependencies

--[no-]slick-greeter
   Use slick-greeter instead of lightdm-gtk-greeter

Wireless configuration
----------------------------

--wifi-ssid WIFI_SSID
  After the -U option configures the ssid of the wifi

--wifi-pass WIFI_PASS
   After the -U option configures the password of the wifi

SSH connection to the server
-----------------------------------

-u USER, --user USER
   sudo-capable user on the server.

-k SSH_KEY, --ssh-key SSH_KEY
   path to an ssh key

Logging and dry-run
------------------------

--dry-run
   Simulate action (also immediately apply pending changes from the
   server).

--syslog
   Send logginf output to syslog

--debug
   Enable debug output

-v, --verbose
   Enable verbose output

COPYRIGHT
=========

Copyright © 2006-2023 The Fuss Project

AUTHORS
=======

Simone Piccardi <piccardi@truelite.it>
Christopher R. Gabriel <cgabriel@truelite.it>
Elena Grandi <elena@truelite.it>
Claudio Cavalli <ccavalli@fuss.bz.it>

LICENSE
=======

Permission to use, copy, modify, and distribute this software and its
documentation for any purpose and without fee is hereby granted,
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in
supporting documentation.
